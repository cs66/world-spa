from flask import Flask, render_template, request
import json
from time import sleep

world = json.load(open("worldl.json"))

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/api/country/<int:countryId>", methods = ["GET","PUT","DELETE"])
def country(countryId):
    global world
    if (request.method == "GET"):
        return world[countryId]
    if (request.method == "PUT"):
        print(f"Update the database with: {request.json}")
        for f in request.json:
            #We must replace html characters to avoid script injection
            world[countryId][f] = request.json[f].replace("<","&lt;")
        return {}
    if request.method == "DELETE":
        del world[countryId]
        return {"success":True}


@app.route("/api/allNames")
def allNames():
    return {"result":[{"id":w["id"],"name":w["name"]} for w in world]}

app.run(debug=True)
