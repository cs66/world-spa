//Fill the dropdown with all countries
document.body.onload = async ()=>{
    let listOfNames0 = await fetch('/api/allNames');
    let listOfNames = await listOfNames0.json();
    listOfNames = [{name:"Select a country"}, ...listOfNames.result];
    document.getElementById("listOfCountries").append(... listOfNames.map(p => {
        let o = document.createElement('option');
        o.innerText = p.name;
        o.value = p.id;
        return o;
    }));
    document.getElementById("listOfCountries").onchange = e=>{
      document.getElementById("countryId").value = e.currentTarget.value;
      document.getElementById("go").click();
    }
}


// Get country details
document.getElementById("go").onclick = async ()=>{
  let countryId = document.getElementById("countryId").value;
  document.body.classList.add("wait");
  try{
    let r0 = await fetch(`/api/country/${countryId}`);
    let r  = await r0.json();
    document.getElementById('countryDetail').innerHTML = `
            <table>
            <tr><td>Id:</td><td class='id'>${r.id}</td></tr>
            <tr><td>Name:</td><td class='name'>${r.name}</td></tr>
            <tr><td>Capital:</td><td class='capital'>${r.capital}</td></tr>
            <tr><td>Continent:</td><td class='continent'>${r.continent}</td></tr>
            <tr><td>Population:</td><td class='population'>${r.population}</td></tr>
            <tr><td>Flag:</td><td><img src=${r.flag}></td></tr>
            </table>
        `;
    let editButton = document.createElement('button');
    editButton.innerText = "Edit";
    editButton.onclick = ()=>{
        document.querySelector('#countryDetail .id').innerHTML =
          `<input value='${document.querySelector('#countryDetail .id').innerText}'/>`;
          document.querySelector('#countryDetail .name').innerHTML =
          `<input value='${document.querySelector('#countryDetail .name').innerText}'/>`;
        document.querySelector('#countryDetail .continent').innerHTML =
          `<input value='${document.querySelector('#countryDetail .continent').innerText}'/>`;
        document.querySelector('#countryDetail .capital').innerHTML =
          `<input value='${document.querySelector('#countryDetail .capital').innerText}'/>`;
        document.querySelector('#countryDetail .population').innerHTML =
          `<input value='${document.querySelector('#countryDetail .population').innerText}'/>`;
        saveBtn.disabled = false;
    }
    let saveBtn = document.createElement('button');
    saveBtn.innerText = "Save";
    saveBtn.disabled = true;
    saveBtn.onclick = ()=>{
        let payload = {
            name:document.querySelector('#countryDetail .name input').value,
            capital:document.querySelector('#countryDetail .capital input').value,
            continent:document.querySelector('#countryDetail .continent input').value,
            id:document.querySelector('#countryDetail .id input').value,
            population:document.querySelector('#countryDetail .population input').value,
        }
        console.log(payload);
        fetch(`/api/country/${countryId}`,{method:'PUT', body:JSON.stringify(payload),headers:{'content-type':'application/json'}});
    }
    let delBtn = document.createElement('button');
    delBtn.innerText = 'Delete';
    delBtn.onclick = ()=>{
        fetch(`/api/country/${countryId}`, {method:'DELETE'})
    }
    document.getElementById('countryDetail').append(editButton, ' ', saveBtn, ' ', delBtn);
  }catch(err){
    alert(err);
  }
  document.body.classList.remove("wait");
}

document.getElementById('prevBtn').onclick = idChangeHandler(-1);
document.getElementById('nextBtn').onclick = idChangeHandler(1);

function idChangeHandler(delta){
    return (()=>{
        document.getElementById("countryId").value = parseInt(document.getElementById("countryId").value) + delta;
        document.getElementById("listOfCountries").value = document.getElementById("countryId").value;
        document.getElementById("go").click();    
    })
}

document.getElementById("countryId").onkeyup = e=>{
    if (e.code==="Enter"){}
}