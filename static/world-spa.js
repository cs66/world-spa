document.getElementById("go").onclick = ()=>{
  let countryId = document.getElementById("countryId").value;
  document.body.classList.add("wait");
  console.log('fetch kicks off');
  fetch(`/api/country/${countryId}`)
    .then(r=>r.json())
    .then(r=>{
      document.getElementById('countryDetail').innerHTML = `
        <table>
        <tr><td>Name:</td><td>${r.name}</td></tr>
        <tr><td>Capital:</td><td>${r.capital}</td></tr>
        <tr><td>Continent:</td><td>${r.continent}</td></tr>
        <tr><td>Flag:</td><td><img src=${r.flag}></td></tr>
        </table>

      `;
      console.log('waiting is over');
      document.body.classList.remove("wait");  
    })
    .catch(err=>{
      alert("Sorry there was a problem");
      document.body.classList.remove("wait");
      console.log(err);
    });
}

